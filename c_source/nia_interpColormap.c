/* File: nia_interpColormap.c 
 *
 * This function implements a linear colormap interpolation.
 *
 * This can be compiled on linux using this command:
 * mex -largeArrayDims COMPFLAGS="$COMPFLAGS -O2 -std=c99" nia_interpColormap.c
 *
 * This can be compiled on windows using this command:
 * mex -largeArrayDims -g COMPFLAGS="$COMPFLAGS /O2" nia_interpColormap.c
 */

#if defined(_WIN32) || defined(_WIN64)
#define NIA_RESTRICT __restrict
#else
#define NIA_RESTRICT restrict
#endif

#include <stdlib.h>
#include <math.h>

#include "mex.h"


/**
 * The following function implements a nearest-neighbor interpolation of
 * the passed pixel values based on the input argument.
 *
 * \param input_vals Array of input values
 * \param input_len Length of input array
 * \param cmap_vals Array of colormap values (r array, g array, b array)
 * \param cmap_len Number of rows in colormap
 * \param zrange Array of input limits (2 elements)
 * \param nan_color Color to use when input is NaN
 * \param output_vals Array of output values
 */
void
nia_interpColormap(double* NIA_RESTRICT input_vals, size_t input_len,
  const double* NIA_RESTRICT cmap_vals, size_t cmap_len, double* NIA_RESTRICT zrange,
  const double* NIA_RESTRICT nan_color, double* NIA_RESTRICT output_vals)
{
   double step_size = (zrange[1] - zrange[0]) / (cmap_len - 1);
   
   for (size_t pixel_idx = 0; pixel_idx < input_len; pixel_idx++) {
      double pixel_val = input_vals[pixel_idx];

      if (pixel_val != pixel_val /* is nan*/) {
          output_vals[pixel_idx + 0*input_len] = nan_color[0];
          output_vals[pixel_idx + 1*input_len] = nan_color[1];
          output_vals[pixel_idx + 2*input_len] = nan_color[2];
          continue;
      }

      long x_rel = (long)((pixel_val - zrange[0])/step_size + 0.5); 

      if (x_rel < 0) x_rel = 0;
      if (x_rel > cmap_len - 1) x_rel = cmap_len - 1;

      output_vals[pixel_idx + 0*input_len] = cmap_vals[x_rel + 0*cmap_len];
      output_vals[pixel_idx + 1*input_len] = cmap_vals[x_rel + 1*cmap_len];
      output_vals[pixel_idx + 2*input_len] = cmap_vals[x_rel + 2*cmap_len];
   }      
}


/**
 * The following function is the gateway from the matlab
 * interface.
 *
 * The matlab function takes three arguments:
 *   input [Npixels x 1], cmap [Ncmap x 3],
 *   zrange [1x2], and nan_color
 *
 * \params nlhs Number of output arguments
 * \params plhs Array of output arguments
 * \param nrhs Number of input arguments
 * \params prhs Array of input arguments
 */
void mexFunction(
   int nlhs, mxArray *plhs[], 
   int nrhs, const mxArray *prhs[])
{
   double *input_vals;
   size_t input_len;
   
   double *cmap_vals;
   size_t cmap_len;

   double *zrange_vals;

   double *nan_color;
   
   mwSize output_dims[3];
   double *output_vals;

   /* check input arguments */
   if (nlhs != 1) {
      mexErrMsgIdAndTxt("nia_interpColormap:nlhs",
        "Must have one output argument");
   }

   if (nrhs != 4) {
      mexErrMsgIdAndTxt("nia_interpColormap:nrhs",
        "Must have four input arguments");
   }

   if (mxGetClassID(prhs[0]) != mxDOUBLE_CLASS || mxIsComplex(prhs[0]) ||
       mxGetNumberOfDimensions(prhs[0]) != 2) {
      mexErrMsgIdAndTxt("nia_interpColormap:input",
        "The argument 'input' must be a matrix of double values");
   }

   input_vals = (double*)mxGetData(prhs[0]);
   input_len = mxGetNumberOfElements(prhs[0]);
   
   if (mxGetClassID(prhs[1]) != mxDOUBLE_CLASS || mxIsComplex(prhs[1]) ||
       mxGetNumberOfDimensions(prhs[1]) != 2 ||
       mxGetM(prhs[1]) < 1 || mxGetN(prhs[1]) != 3) {
      mexErrMsgIdAndTxt("nia_interpColormap:cmap",
        "The argument 'cmap' must be a [nx3] matrix of double values");
   }

   cmap_vals = (double*)mxGetData(prhs[1]);
   cmap_len = mxGetM(prhs[1]);
   
   if (mxGetClassID(prhs[2]) != mxDOUBLE_CLASS || mxIsComplex(prhs[2]) ||
       mxGetNumberOfDimensions(prhs[2]) != 2 ||
       mxGetM(prhs[2]) != 1 || mxGetN(prhs[2]) != 2) {
      mexErrMsgIdAndTxt("nia_interpColormap:zrange",
        "The argument 'zrange' must be a [1x2] matrix of double values");
   }

   zrange_vals = (double*)mxGetData(prhs[2]);

   if (mxGetClassID(prhs[3]) != mxDOUBLE_CLASS || mxIsComplex(prhs[3]) ||
       mxGetNumberOfDimensions(prhs[3]) != 2 ||
       mxGetM(prhs[3]) != 1 || mxGetN(prhs[3]) != 3) {
       mexErrMsgIdAndTxt("nia_interpColormap:nan_color",
           "The argument 'nan_color' must be a [1x3] matrix of double values");
   }

   nan_color = (double*)mxGetData(prhs[3]);
   
   /* allocate the output array */
   output_dims[0] = mxGetM(prhs[0]);
   output_dims[1] = mxGetN(prhs[0]);
   output_dims[2] = 3;
   
   plhs[0] = mxCreateNumericArray(3, output_dims, mxDOUBLE_CLASS, mxREAL);
   if (plhs[0] == NULL) {
      mexErrMsgIdAndTxt("nia_interpColormap:internal",
        "An internal error has occurred");
      return;
   }

   output_vals = mxGetData(plhs[0]);

   /* compute the hash */
   nia_interpColormap(input_vals, input_len,
      cmap_vals, cmap_len, zrange_vals, nan_color, output_vals);
}


