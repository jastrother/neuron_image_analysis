/* File: nia_getConnectedRegions.c 
 *
 * This function implements a faster version of bwlabel
 *
 * This can be compiled on linux using this command:
 * mex -largeArrayDims COMPFLAGS="$COMPFLAGS -O2 -std=c99" nia_getConnectedRegions.c
 *
 * This can be compiled on windows using this command:
 * mex -largeArrayDims -g COMPFLAGS="$COMPFLAGS /O2" nia_getConnectedRegions.c
 */


#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
typedef unsigned __int8 uint8_t;
typedef unsigned __int64 uint64_t;
#define NIA_UNLIKELY(X) (X)
#define NIA_RESTRICT __restrict
#else
#include <stdint.h>
#define NIA_UNLIKELY(X) (__builtin_expect((X),0))
#define NIA_RESTRICT restrict
#endif

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "mex.h"


/* This type is used to store pixel values */
typedef uint8_t nia_imconn_pixel_t;

/* This type is used for summing pixel coordinates */
typedef uint64_t nia_imconn_big_t;

/* This type is used for the body identifier */
typedef size_t nia_imconn_id_t;

/* The following type is used to store the information
 * for a single connected body.
 */
typedef struct {
  nia_imconn_big_t x_tot;
  nia_imconn_big_t y_tot;
  size_t t_area;
  double x_avg;
  double y_avg;
} nia_imconn_body_t;


/* The following type is used to store the information for
 * a remap entry.
 */
typedef struct
{
  /* The following member stores a map from an old identifier
   * to a new identifier. The Nth element stores the id number
   * that the id N should be mapped to.
   */
  nia_imconn_id_t* id_map;

  /* The following member stores a reverse lookup of all of the
   * id numbers that map to a given id. The Nth element stores
   * the head of a linked list that contains the id numbers that
   * map to the id number N.
   */
  nia_imconn_id_t* rev_map;

  /* The following member contains data on whether the passed
   * identifier is real (should be output) or virtual (should
   * not be output). The Nth element is one if the N identifier
   * is real and zero otherwise.
   */
  uint8_t* output_map;

  /* The following member stores the number of elements in the
   * remap that are currently being used.
   */
  size_t used_length;

  /* The following member stores the allocated length of each
   * of the remap arrays.
   */
  size_t alloc_length;
} nia_imconn_remap_t;


/* The following enumeration lists out errors
 */
enum {
   NIA_IMCONN_EOKAY   = 0,
   NIA_IMCONN_EMEMORY = -1,
   NIA_IMCONN_EBOUNDS = -2
};


/*
 * The following macros exist for internal use
 *****************************************************
 */


/* This function initializes the remap memory.
 */
void
nia_imconn_remap_init(
   nia_imconn_remap_t* remap)
{
   remap->id_map = NULL;
   remap->rev_map = NULL;
   remap->output_map = NULL;
   remap->used_length = 0;
   remap->alloc_length = 0;
}


/* This function frees the remap memory
 */
void
nia_imconn_remap_free(
   nia_imconn_remap_t* remap)
{
   free(remap->id_map);
   free(remap->rev_map);
   free(remap->output_map);
}


/* This function frees the remap
 */
static void
nia_imconn_remap_clear(
   nia_imconn_remap_t* remap)
{
   remap->used_length = 0;
}


/* The following function reallocates the re-map
 * so that has at least the given length.  It
 * does initialize new memory.  It returns -1 if
 * it runs out of memory and 0 otherwise.
 */
static int
nia_imconn_remap_grow(
   size_t new_len,
   nia_imconn_remap_t* remap)
{
   size_t i;

   if (new_len > remap->alloc_length) {
      nia_imconn_id_t* tmp;
      uint8_t* tmp2;
      size_t mem_len;

      mem_len = 2 * remap->alloc_length;
      mem_len = mem_len > new_len ? mem_len : new_len;

      tmp = (nia_imconn_id_t*)realloc(remap->id_map,
         mem_len * sizeof(nia_imconn_id_t));
      if (tmp == NULL) return -1;
      remap->id_map = tmp;

      tmp = (nia_imconn_id_t*)realloc(remap->rev_map,
         mem_len * sizeof(nia_imconn_id_t));
      if (tmp == NULL) return -1;
      remap->rev_map = tmp;

      tmp2 = (uint8_t*)realloc(remap->output_map,
         mem_len * sizeof(uint8_t));
      if (tmp2 == NULL) return -1;
      remap->output_map = tmp2;

      for (i=remap->used_length; i<new_len; i++) {
         remap->id_map[i] = 0;
         remap->rev_map[i] = 0;
         remap->output_map[i] = 1;
      }

      remap->used_length = new_len;
      remap->alloc_length = mem_len;

   } else if (new_len > remap->used_length) {

      for (i=remap->used_length; i<new_len; i++) {
         remap->id_map[i] = 0;
         remap->rev_map[i] = 0;
         remap->output_map[i] = 1;
      }

      remap->used_length = new_len;
   }

   return 0;
}


/* The following function maintains a map to place
 * the two id numbers into the same body. The task
 * becomes complicated when an id number that has
 * been mapped *to* is overwritten.  In this case
 * the map cannot simply be overwritten, but all
 * entries which point *to* the modified entry need
 * to be modified to reflect a new body id.  To do
 * this and avoid having to scan the entire list a
 * separate reverse map is maintained.  This map is a
 * linked list where the ith entry points to the id
 * of the next entry in the same group. The function
 * will return -1 on error and 0 otherwise.
 */
static int
nia_imconn_remap_set(
   nia_imconn_remap_t* remap,
   nia_imconn_id_t* cur_pos,
   nia_imconn_id_t id_one, 
   nia_imconn_id_t id_two)
{
   nia_imconn_id_t *id_map, *rev_map;
   uint8_t* output_map;
   nia_imconn_id_t pt1, pt2;

   /* shortcut when nothing to do */
   if (id_one == id_two) return 0;

   /* the remap is not grown every time an identifier
    * is added, so we need to grow it now so that 
    * elements exist for every current identifier
    */
   if (nia_imconn_remap_grow(*cur_pos+1, remap)) {
      return -1;
   }

   /* arrange helpful handles */
   id_map = remap->id_map;
   rev_map = remap->rev_map;
   output_map = remap->output_map;
   pt1 = id_map[id_one];
   pt2 = id_map[id_two];

   /* shortcut when nothing to do */
   if (pt1 != 0 && pt1 == pt2) return 0;

   /* merge the two identifiers based on
    * if they have already been mapped or
    * not mapped at all. because we only
    * map to new id numbers we know that
    * no one will ever try to re-map a
    * mapped-to identifier.
    */
   if (pt1 > 0) {
      if (pt2 > 0) {
         /* Both id_one and id_two have
          * been mapped.  We scan reverse map 
          * for id_two to replace with
          * group for id_one.
          */
         nia_imconn_id_t t1, t2;

         t2 = t1 = rev_map[pt2];
         while (t1 != 0) {
            id_map[t1] = pt1;
            t2 = t1;
            t1 = rev_map[t1];
         }

         rev_map[t2] = rev_map[pt1];
         rev_map[pt1] = rev_map[pt2];
         rev_map[pt2] = 0;
      } else {
         /* id_one is mapped but id_two is
          * not.  We insert id_two into the
          * id_one reverse map.
          */
         id_map[id_two] = pt1;
         pt2 = rev_map[pt1];
         rev_map[pt1] = id_two;
         rev_map[id_two] = pt2;
      }
   } else {
      if (pt2 > 0) {
         /* id_two is mapped but id_one is
          * not.  We insert id_one into the
          * id_two reverse map.
          */
         id_map[id_one] = pt2;
         pt1 = rev_map[pt2];
         rev_map[pt2] = id_one;
         rev_map[id_one] = pt1;
      } else {
         /* Neither id_one or id_two have
          * been mapped.  We generate new
          * group identifier number.
          */
         pt1 = ++(*cur_pos);

         /* grow the re-map if necessary */
         if (nia_imconn_remap_grow(pt1+1, remap)) {
            return -1;
         }

         /* handles may have changed */
         id_map = remap->id_map;
         rev_map = remap->rev_map;
         output_map = remap->output_map;

         /* finally reconnect maps */
         output_map[pt1] = 0;
         id_map[pt1] = 0;
         id_map[id_one] = pt1;
         id_map[id_two] = pt1;
         rev_map[pt1] = id_one;
         rev_map[id_one] = id_two;
         rev_map[id_two] = 0;
      }
   }

   return 0;
}


/* The following function reduces a re-mapping
 * to a unique map whose id numbers are packed.
 * It returns the number of packed identifiers.
 *
 */
static size_t
nia_imconn_remap_finish(
   nia_imconn_remap_t* remap)
{
   nia_imconn_id_t* id_map = remap->id_map;
   nia_imconn_id_t* rev_map = remap->rev_map;
   uint8_t* output_map = remap->output_map;
   nia_imconn_id_t cur_pos = 0;
   size_t i;

   /* we don't need the reverse map anymore,
    * so we'll reuse them to store packed id
    * numbers of mapped-to numbers
    */
   memset(rev_map, 0, remap->used_length
      * sizeof(nia_imconn_id_t));

   /* go from arbitrary numbers into a
    * set of tightly packed identifers
    */
   for (i = 1; i < remap->used_length; i++) {
      nia_imconn_id_t id;

      if (!output_map[i]) continue;

      id = id_map[i];
      if (id > 0) {
         nia_imconn_id_t tmp = rev_map[id];

         if (tmp == 0) {
            rev_map[id] = ++cur_pos;
            id_map[i] = cur_pos;
         } else {
            id_map[i] = tmp;
         }
      } else {
         id_map[i] = ++cur_pos;
      }
   }

   return cur_pos+1;
}


/* The following function maps from the first-pass
 * mapping into a correct, packed mapping.
 */
static void
nia_imconn_remap_apply(
   nia_imconn_id_t * map,
   size_t nrow,
   size_t ncol,
   nia_imconn_body_t* bodies,
   size_t bodies_num,
   nia_imconn_remap_t* remap)
{
   nia_imconn_id_t* id_map = remap->id_map;
   size_t i, j;

   if (bodies == NULL) {
      for (i = 0; i < nrow; i++) {
         nia_imconn_id_t* map_r = map + i*ncol;

         for (j = 0; j < ncol; j++) {
            map_r[j] = id_map[map_r[j]];
         }
      }
   } else {
      for (i = 0; i < bodies_num; i++) {
         bodies[i].t_area = 0;
         bodies[i].x_tot = 0;
         bodies[i].y_tot = 0;
         bodies[i].x_avg = 0.0;
         bodies[i].y_avg = 0.0;
      }

      for (i = 0; i < nrow; i++) {
         nia_imconn_id_t* map_r = map + i*ncol;

         for (j = 0; j < ncol; j++) {
            nia_imconn_id_t tmp = map_r[j];
            if (tmp > 0) {
               nia_imconn_id_t id = id_map[tmp];
               map_r[j] = id;
               bodies[id].t_area += 1;
               bodies[id].x_tot += j;
               bodies[id].y_tot += i;
            }
         }
      }

      for (i = 1; i < bodies_num; i++) {
         bodies[i].x_avg = bodies[i].x_tot / (double)bodies[i].t_area;
         bodies[i].y_avg = bodies[i].y_tot / (double)bodies[i].t_area;
      }
   }
}


/* The following function finds connected pixels.
 * It takes an image with dimensions, a threshold
 * used to determine if a pixel is on, a map that
 * is the same size as the image and will contain
 * the body number for a pixel on completion, a
 * pointer to an array of body boxes, the size of
 * the array of body boxes, and a pointer to remap 
 * memory.
 *
 * The calling convention for the bodies is a
 * little complicated to allow multiple invocation
 * methods. To pass a fixed size array simply pass
 * a pointer to the array and then the max length.
 * To request dynamic allocation, pass a pointer to
 * a pointer whose value is NULL;  the pointer will
 * be overwritten with the address of the allocated
 * memory. To avoid calculating any body statistics
 * pass NULL as the value of bodies.
 *
 * The remap argument is optional, if it is left as
 * null then the required memory will be allocated
 * dynamically.  If it is provided then it must be
 * initialized with nia_imconn_remap_init and, after it
 * is no longer needed, freed with nia_imconn_remap_free.
 * 
 * This function returns 0 on success and a negative
 * error code on failure.
 */
int nia_imconn_make(
   nia_imconn_pixel_t* im,
   size_t nrow,
   size_t ncol,
   nia_imconn_pixel_t thres,
   nia_imconn_id_t* map,
   nia_imconn_body_t** bodies,
   size_t* bodies_max,
   nia_imconn_remap_t* remap)
{
   nia_imconn_id_t cur_pos = 0;
   size_t i, j, bodies_num;
   int status = NIA_IMCONN_EOKAY;
   nia_imconn_remap_t remap_x;
   nia_imconn_body_t *bodies_ptr;


   /* initialize the remap members.
    */
   if (remap == NULL) {
      remap = &remap_x;
      nia_imconn_remap_init(remap);
   } else {
      nia_imconn_remap_clear(remap);
   }

   /* upper-left corner pixel */
   if (im[0] > thres) {
      map[0] = ++cur_pos;
   } else {
      map[0] = 0;
   }

   /* upper-most pixel row */
   for (i = 1; i < ncol; i++) {
      if (im[i] > thres) {
         nia_imconn_id_t id = map[i-1];

         if (id > 0) {
           map[i] = id;
         } else {
           map[i] = ++cur_pos;
         }
      } else {
         map[i] = 0;
      }
   }

   /* general case for all other rows */
   for (i = 1; i < nrow; i++) {
      nia_imconn_pixel_t* im_r = im + i*ncol;
      nia_imconn_id_t* map_r = map + i*ncol;

      /* left-most pixel column */
      if (im_r[0] > thres) {
         nia_imconn_id_t id = map_r[-ncol];
                                                                                                 
         if (id > 0) {
            map_r[0] = id;
         } else {
            map_r[0] = ++cur_pos;
         }
      } else {
         map_r[0] = 0;
      }

      /* general case for all pixels */
      for (j = 1; j < ncol; j++) {
         if (NIA_UNLIKELY(im_r[j] > thres)) {
            nia_imconn_id_t id1, id2;
 
            id1 = map_r[j - 1];
            id2 = map_r[j - ncol];

            if (id1 > 0) {
               map_r[j] = id1;

               /* if both left and up ids exist,
                * must create entry to do remap
                */
               if (id2 > 0 && id1 != id2) {
                   if (nia_imconn_remap_set(remap,
                        &cur_pos, id1, id2)) {
                      return NIA_IMCONN_EMEMORY; 
                   }
               }
            } else {
               /* left doesn't exist, so use up
                * id if it exists or make new id
                */
               if (id2 > 0) {
                  map_r[j] = id2;
               } else {
                  map_r[j] = ++cur_pos;
               }
            }
         } else {
            map_r[j] = 0;
         }
      }
   }

   /* finish the re-mapping generated */
   nia_imconn_remap_grow(cur_pos+1, remap);
   bodies_num = nia_imconn_remap_finish(remap);

   /* check range and allocate memory.
    * kind of twisty-turny, but this is
    * necessarily so
    */
   if (bodies != NULL) {
      if (*bodies == NULL) {
         *bodies = (nia_imconn_body_t*)malloc(
            bodies_num * sizeof(nia_imconn_body_t));
         if (*bodies == NULL) {
            status = NIA_IMCONN_EMEMORY;
            goto finish;
         }
      } else if (bodies_num > *bodies_max) {
         status = NIA_IMCONN_EBOUNDS;
         goto finish;
      }

      bodies_ptr = *bodies;
      *bodies_max = bodies_num;
   } else {
      bodies_ptr = NULL;

      if (bodies_max != NULL) {
         *bodies_max = bodies_num;
      }
   }

   /* apply the re-map original identifiers. 
    * we reuse this pass to calculate
    * the body statistics.
    */
   nia_imconn_remap_apply(map, nrow, ncol,
       bodies_ptr, bodies_num, remap);

finish:
   /* free the generated re-mapping */
   if (remap == &remap_x) {
      nia_imconn_remap_free(remap);
   }

   return status;
} 


/* The following function verifies the map which
 * was produced as a result from nia_imconn_make()
 */
int
nia_imconn_verify(
   nia_imconn_pixel_t* im,
   size_t nrow,
   size_t ncol,
   nia_imconn_pixel_t thres,
   nia_imconn_id_t* map,
   nia_imconn_body_t* bodies,
   size_t bodies_num)
{
   size_t i, j;

   for (i = 0; i < nrow; i++) {
      nia_imconn_pixel_t* im_r = im+i*ncol;
      nia_imconn_id_t* map_r = map+i*ncol;

      for (j = 0; j < ncol; j++) {
         if (im_r[j] > thres) {
            nia_imconn_id_t tmp;

            if (map_r[j] == 0)
               return -1;

            tmp = map_r[j-1];
            if (j > 0 && tmp > 0 &&
                map_r[j] != tmp)
               return -2;

            tmp = map[(i-1)*ncol+j];
            if (i > 0 && tmp > 0 &&
                map_r[j] != tmp)
               return -3;
         }
      }
   }

   if (bodies != NULL) {
      if (bodies[0].t_area != 0)
         return -4;

      for (i = 1; i < bodies_num; i++) {
         if (bodies[i].t_area == 0)
            return -4;

         if (bodies[i].x_avg < 0 ||
             bodies[i].x_avg > ncol)
            return -5;

         if (bodies[i].y_avg < 0 ||
             bodies[i].y_avg > nrow)
            return -6;
      }
   }

   return 0;
}


/* This function interfaces matlab with the nia_imconn code
 */
void
nia_imconn_go(
  uint8_t* image,
  size_t nrow,
  size_t ncol,
  mxArray** labels_p,
  mxArray** props_p)
{
   int props_num_fieldnames = 2;
   const char* props_fieldnames[] = {
      "area", "centroid"};

   mxArray* labels;
   mxArray* props;
   nia_imconn_id_t *map;
   nia_imconn_body_t* bodies;
   size_t bodies_num = 0;
   double *labels_vals;
   size_t i;

   /* allocate memory for output map */
   map = (nia_imconn_id_t*)malloc(nrow*ncol*sizeof(nia_imconn_id_t));
   if (map == NULL) {
      mexErrMsgIdAndTxt("nia_getConnectedRegions:internal",
        "An internal error has occurred");
      return;
   }

   /* run the region connectivity analysis */
   bodies = NULL;
   if (nia_imconn_make(image, nrow, ncol, 0,
         map, &bodies, &bodies_num, NULL)) {
      mexErrMsgIdAndTxt("nia_getConnectedRegions:internal",
        "An internal error has occurred");
      return;
   }

   /* allocate the output array */
   /* flip rows and columns to reflect C vs MATLAB arrays */
   labels = *labels_p = mxCreateDoubleMatrix(ncol, nrow, mxREAL);
   if (labels == NULL) {
      mexErrMsgIdAndTxt("nia_getConnectedRegions:internal",
        "An internal error has occurred");
      return;
   }

   labels_vals = mxGetPr(labels);

   /* convert map to doubles */
   for (i = 0; i < nrow*ncol; i++) {
      labels_vals[i] = (double)map[i];
   }
  
   free(map);

   /* convert bodies struct to matlab struct */
   props = *props_p = mxCreateStructMatrix(bodies_num, 1,
     props_num_fieldnames, props_fieldnames);
   if (props == NULL) {
      mexErrMsgIdAndTxt("nia_getConnectedRegions:internal",
        "An internal error has occurred");
      return;
   }

   for (i = 0; i < bodies_num; i++) { 
      mxArray* mx_area;
      mxArray* mx_centroid;
      double* centroid_vals;

      mx_area = mxCreateDoubleMatrix(1, 1, mxREAL);
      *mxGetPr(mx_area) = bodies[i].t_area;
      mxSetFieldByNumber(props, i, 0, mx_area);

      mx_centroid = mxCreateDoubleMatrix(2, 1, mxREAL);
      centroid_vals = mxGetPr(mx_centroid);

      /* flip rows and columns to reflect C vs MATLAB arrays */
      centroid_vals[0] = bodies[i].y_avg + 1; /* one-based index */
      centroid_vals[1] = bodies[i].x_avg + 1; /* one-based index */
      mxSetFieldByNumber(props, i, 1, mx_centroid);
   }

   free(bodies);
}


/**
 * The following function is the gateway from the matlab
 * interface.
 *
 * The matlab function takes one argument:
 *   im (uint8_t, zero or one)
 *
 * The matlab function has two outputs:
 *   labels (double, same size as im)
 *   props (struct array)
 *
 * \params nlhs Number of output arguments
 * \params plhs Array of output arguments
 * \param nrhs Number of input arguments
 * \params prhs Array of input arguments
 */
void mexFunction(
   int nlhs, mxArray *plhs[], 
   int nrhs, const mxArray *prhs[])
{
   uint8_t* image_vals;

   /* check input arguments */
   if (nlhs != 2) {
      mexErrMsgIdAndTxt("nia_getConnectedRegions:nlhs",
        "Must have two output arguments");
   }

   if (nrhs != 1) {
      mexErrMsgIdAndTxt("nia_getConnectedRegions:nrhs",
        "Must have one input argument");
   }

   if (mxGetClassID(prhs[0]) != mxUINT8_CLASS || 
       mxGetNumberOfDimensions(prhs[0]) != 2) {
      mexErrMsgIdAndTxt("nia_getConnectedRegions:image",
        "The argument 'image' must be a matrix of uint8 values");
   }

   image_vals = mxGetData(prhs[0]);

   /* flip rows and column to reflect C vs MATLAB arrays */
   nia_imconn_go(image_vals, mxGetN(prhs[0]), mxGetM(prhs[0]),
     &plhs[0], &plhs[1]);
}
 

