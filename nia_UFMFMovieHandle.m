classdef nia_UFMFMovieHandle < handle
    %nia_UFMFMovieHandle Handle for UFMF Movie
    %   This class is used by nia_playUFMFMovie to maintain a persistent
    %   copy of the UFMF header information.
    
    properties
        % Filename for UFMF movie
        filename;
        
        % Header information for UFMF movie
        header;
    end
    
    methods
    end
    
end

