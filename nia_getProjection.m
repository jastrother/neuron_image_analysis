function output = nia_getProjection(V, dims, angles, mode, in_limits)
% This function calculates a volumetric maximum intensity projection.
% Programs like FluoRender are much faster, but this can be useful when
% speed isn't critical. It accepts the following arguments:
% 
%   V - volumetric data, rows=x, cols=y, 3rd dim=z
%   dims - length of volume in X, Y, Z dimensions
%   angles - rotation of volume around X, Y, Z axes (extrinsic)
%   mode - run mode
%     'get_limits' - return XY projection limits
%     'set_limits' - return projection, use passed in_limits
%     'find_limits' - return projection, calculate limits


% Create gridded interpolant
interp_fun = griddedInterpolant(...
    {linspace(-dims(1)/2, dims(1)/2, size(V,1)), ...
    linspace(-dims(2)/2, dims(2)/2, size(V,2)), ...
    linspace(-dims(3)/2, dims(3)/2, size(V,3))}, ...
    V, 'linear', 'none');

% Find the minimum voxel dimension
voxel_length = min(dims ./ size(V));

% Calculate transform to projection space
X_rot_mat = [               1,               0,                0; ...
                        0,  cos(angles(1)),  sin(angles(1)); ...
                        0  -sin(angles(1)),  cos(angles(1))];                

Y_rot_mat = [  cos(angles(2)),               0, -sin(angles(2)); ...
                        0,               1,               0; ...
           sin(angles(2)),               0,  cos(angles(2))];

Z_rot_mat = [  cos(angles(3)),  sin(angles(3)),               0; ...
          -sin(angles(3)),  cos(angles(3)),               0; ...
                        0,               0,               1];
                    
XYZ_rot_mat = Y_rot_mat * X_rot_mat * Z_rot_mat;

                    
% Find positions of corners of dataset in unrotated coordinate system
[corners_x, corners_y, corners_z] = ndgrid(...
    [-dims(1)/2, dims(1)/2], [-dims(2)/2, dims(2)/2], [-dims(3)/2, dims(3)/2]);

% Transform corner points into rotated coordinate system
corners_trans = XYZ_rot_mat * [corners_x(:)'; corners_y(:)'; corners_z(:)'];

% Use corners to find bounds in rotated coordinate system
switch mode
    case 'get_limits'
        xy_limits = zeros(3,2);

        for idx=1:2
            min_pt = min(corners_trans(idx,:));
            max_pt = max(corners_trans(idx,:));
            extent = max(abs([min_pt, max_pt]));

            xy_limits(idx,1) = -extent;
            xy_limits(idx,2) = extent;
        end

        output = xy_limits(1:2,:);
        return;
    case 'set_limits'
        bounds_trans = zeros(3,3,2); % (x,y,z coords), (x,y,z axes), (min,max)
        for idx=1:2
            bounds_trans(:,idx,1) = 0;
            bounds_trans(idx,idx,1) = in_limits(idx,1);
            
            bounds_trans(:,idx,2) = 0;
            bounds_trans(idx,idx,2) = in_limits(idx,2);
        end
        
        for idx=3
            min_pt = min(corners_trans(idx,:));
            max_pt = max(corners_trans(idx,:));
            extent = max(abs([min_pt, max_pt]));
            
            bounds_trans(:,idx,1) = 0;
            bounds_trans(idx,idx,1) = -extent;
            
            bounds_trans(:,idx,2) = 0;
            bounds_trans(idx,idx,2) = +extent;
        end
    case 'find_limits'
        bounds_trans = zeros(3,3,2); % (x,y,z coords), (x,y,z axes), (min,max)
        for idx=1:3
            min_pt = min(corners_trans(idx,:));
            max_pt = max(corners_trans(idx,:));
            extent = max(abs([min_pt, max_pt]));
            
            bounds_trans(:,idx,1) = 0;
            bounds_trans(idx,idx,1) = -extent;
            
            bounds_trans(:,idx,2) = 0;
            bounds_trans(idx,idx,2) = +extent;
        end
    otherwise
        error 'unrecognized mode';
end

% Convert bounds points to unrotated coordinate system
bounds_trans = reshape(bounds_trans, 3, 6);
bounds_pts = XYZ_rot_mat \ bounds_trans;
bounds_pts = reshape(bounds_pts, 3, 3, 2);

% Find the number of points for each ray trace direction
trav_origin = sum(bounds_pts(:,:,1),2);
trav_dir_vec = diff(bounds_pts,1,3);

trav_num_pts = zeros(3,1);
for idx=1:3
    trav_num_pts(idx) = ceil(norm(trav_dir_vec(:,idx))/voxel_length);
end

% Ray trace each pixel
proj_im = zeros(trav_num_pts(1), trav_num_pts(2));

traverse_idx3 = 1:trav_num_pts(3);
z_trace = bsxfun(@times, (traverse_idx3-0.5), trav_dir_vec(:,3) / trav_num_pts(3));

for traverse_idx1=1:trav_num_pts(1)
    pos_level1 = trav_origin(:,1) + (traverse_idx1-0.5) * trav_dir_vec(:,1) / trav_num_pts(1);
     
    for traverse_idx2=1:trav_num_pts(2)
        pos_level2 = pos_level1 + (traverse_idx2-0.5) * trav_dir_vec(:,2) / trav_num_pts(2);
        pos_level3 = pos_level2 + z_trace;
               
        ray_values = interp_fun(pos_level3(1,:), pos_level3(2,:), pos_level3(3,:));       
        
        notnan_mask = ~isnan(ray_values);
        if nnz(notnan_mask) == 0
            assigned_val = 0;
        else
            assigned_val = max(ray_values(notnan_mask));
        end
        
        proj_im(traverse_idx1,traverse_idx2) = assigned_val;
    end
end

output = proj_im;

end
