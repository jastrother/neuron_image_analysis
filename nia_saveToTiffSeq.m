function nia_saveToTiffSeq(obj, pos_vec, home_pos, scan_extents, ...
    channel_id, out_fname, im_range)
%NIA_SAVETOTIFFSEQ Scan through movie and save images as TIFF sequence
%   This function scans through the movie using the passed arguments and
%   saves each slice as a frame in a tiff sequence. The 'pos_vec'
%   'home_pos', 'scan_extents', and 'channel_id' parameters are documented
%   in nia_movie_scan. The argument 'out_fname_pattern' is the name of the
%   output file, a slice index will be appended to the name. The argument
%   'im_range' is a 1x2 array that specifies the intensity range to use for
%   the output image, the first element is the minimum intensity and the
%   second element is the maximum.


suffix_pos = find(out_fname == '.', 1, 'last');
if isempty(suffix_pos)
    tiff_pattern = [out_fname, '_%06d.tif'];
else
    tiff_pattern = [out_fname(1:suffix_pos-1), ...
        '_%06d', out_fname(suffix_pos:end)];
end

udata.tiff_pattern = tiff_pattern;
udata.im_range = im_range;
udata.slice_num = 0;

udata = obj.scan(pos_vec, home_pos, scan_extents, channel_id, @saveSlice, udata);

end

function udata = saveSlice(image, ~, udata)
% This function creates a new frame in the multipage tiff.

fname = sprintf(udata.tiff_pattern, udata.slice_num);

tiff_fid = Tiff(fname, 'w');
im_range = udata.im_range;

image = (image - im_range(1))/(im_range(2) - im_range(1));
image(image > 1) = 1.0;
image(image < 0) = 0.0;
image = uint8(image * 255);

tagStruct.Photometric = 1; %min is black
tagStruct.PlanarConfiguration = 1; %contig
tagStruct.BitsPerSample = 8;
tagStruct.SamplesPerPixel = 1;
tagStruct.ImageLength = size(image, 1);
tagStruct.ImageWidth = size(image, 2);
tiff_fid.setTag(tagStruct);
tiff_fid.write(image);

close(tiff_fid);

udata.slice_num = udata.slice_num + 1;

end

