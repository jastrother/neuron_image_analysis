
/*  Example of usage
 ********************************************************************
 */

#undef DO_VERIFY
#undef DO_PRINT

#define NUM_RUNS 50
#define IMG_NROW 1024
#define IMG_NCOL 2000
#define MAX_BODIES 500000

void
print_im(
   nia_imconn_pixel_t* im,
   size_t nrow,
   size_t ncol)
{
   size_t i, j;

   for (i = 0; i < nrow; i++) {
      nia_imconn_pixel_t* im_r = im + i*ncol;

      for (j = 0; j < ncol; j++) {
         printf("%-5d ", im_r[j]);
      }

      printf("\n");
   }
}

void
print_map(
   nia_imconn_id_t* map,
   size_t nrow,
   size_t ncol)
{
   size_t i, j;

   for (i = 0; i < nrow; i++) {
      nia_imconn_id_t* map_r = map + i*ncol;

      for (j = 0; j < ncol; j++) {
         printf("%-5d ", map_r[j]);
      }

      printf("\n");
   }
}

int main()
{
   const int nrow = IMG_NROW;
   const int ncol = IMG_NCOL; 

   nia_imconn_id_t *map;
   size_t i, j, bodies_num;
   nia_imconn_remap_t remap;
   nia_imconn_body_t *bodies;
   nia_imconn_pixel_t* im;

   struct timeval tv_start, tv_stop;
   double total_time;

   im = (nia_imconn_pixel_t*)malloc(nrow*ncol*sizeof(nia_imconn_pixel_t));
   map = (nia_imconn_id_t*)malloc(nrow*ncol*sizeof(nia_imconn_id_t));
   bodies = (nia_imconn_body_t*)malloc(MAX_BODIES*sizeof(nia_imconn_body_t));

   nia_imconn_remap_init(&remap);

#ifndef DO_VERIFY
      for (i = 0; i < nrow * ncol; i++) {
         im[i] = rand() < (RAND_MAX / 4);
      }
#endif

   gettimeofday(&tv_start, NULL);

   for (j = 0; j < NUM_RUNS; j++) {

#ifdef DO_VERIFY
      for (i = 0; i < nrow * ncol; i++) {
         im[i] = rand() < (RAND_MAX / 4);
      }
#endif

      bodies_num = MAX_BODIES;
      if (nia_imconn_make(im, nrow, ncol, 0,
             map, &bodies, &bodies_num, &remap))
      {
         printf("FAILED\n");
         exit(1);
      }

#ifdef DO_PRINT
      printf("IMG:\n");
      print_im(im, nrow, ncol);

      printf("MAP:\n");
      print_map(map, nrow, ncol);

      printf("STATS:\n");
      for (i = 0; i < bodies_num; i++) {
         printf("%-5d %-3d %-6.2f %.2f\n",
           i, bodies[i].t_area,
              bodies[i].x_avg,
              bodies[i].y_avg);
      }
#endif

#ifdef DO_VERIFY
      if (nia_imconn_verify(im, nrow, ncol, 0,
             map, bodies, bodies_num))
      {
         printf("FAILED\n");
         exit(1);
      }
#endif

   }

   nia_imconn_remap_free(&remap);
   free(im);
   free(map);
   free(bodies);

   gettimeofday(&tv_stop, NULL);

   total_time = (tv_stop.tv_sec - tv_start.tv_sec)
     + 1E-6 * (tv_stop.tv_usec - tv_start.tv_usec);
   total_time /= NUM_RUNS;

   printf("NUMBER OF RUNS = %d\n", NUM_RUNS);
   printf("IMAGE SIZE = %d x %d\n", IMG_NROW, IMG_NCOL);
   printf("AVERAGE = %.3f SEC / IMAGE\n", total_time);
   return 0;
}

