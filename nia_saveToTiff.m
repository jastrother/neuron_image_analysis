function nia_saveToTiff(obj, pos_vec, home_pos, scan_extents, ...
    channel_id, out_fname, im_range)
%NIA_SAVETOTIFF Scan through movie and save images as multipage TIFF
%   This function scans through the movie using the passed arguments
%   and saves each slice as a frame in a multipage tiff. The 'pos_vec'
%   'home_pos', 'scan_extents', and 'channel_id' parameters are documented
%   in nia_movie_scan. The argument 'out_fname' is the name of the output
%   file. The argument 'im_range' is a 1x2 array that specifies the
%   intensity range to use for the output image, the first element is the
%   minimum intensity and the second element is the maximum.

tiff_fid = Tiff(out_fname, 'w');

udata.tiff_fid = tiff_fid;
udata.im_range = im_range;

udata = obj.scan(pos_vec, home_pos, scan_extents, channel_id, @saveSlice, udata);
tiff_fid = udata.tiff_fid;

close(tiff_fid);

end

function udata = saveSlice(image, ~, udata)
% This function creates a new frame in the multipage tiff.

tiff_fid = udata.tiff_fid;
im_range = udata.im_range;

image = (image - im_range(1))/(im_range(2) - im_range(1));
image(image > 1) = 1.0;
image(image < 0) = 0.0;
image = uint8(image * 255);

tagStruct.Photometric = 1; %min is black
tagStruct.PlanarConfiguration = 1; %contig
tagStruct.BitsPerSample = 8;
tagStruct.SamplesPerPixel = 1;
tagStruct.ImageLength = size(image, 1);
tagStruct.ImageWidth = size(image, 2);
tiff_fid.setTag(tagStruct);
tiff_fid.write(image);
tiff_fid.writeDirectory();

end

