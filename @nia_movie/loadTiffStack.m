function loadTiffStack(obj, fname)
%LOADTIFFSTACK Read movie from a tiff file

% Check input parameters
if ~nia_isString(fname)
    error 'The argument ''fname'' must be string'
end

% Destroy the movie
obj.slices = [];
obj.ch_list = [];
obj.pos_lens = [];
obj.pos_lu = {};
obj.pos_ranges = {};
obj.hist_info = [];

% If unclean version doesn't exist, then error
if ~exist(fname, 'file')
    error('The file ''%s'' does not exist', fname);
end

% Count the total number of slices
total_slices = 0;

fp = Tiff(fname, 'r');
while true
    total_slices = total_slices + 1;
    
    if fp.lastDirectory()
        break;
    else
        fp.nextDirectory();
    end
end

close(fp);
    
% Initialize channel list and indices
ch_list = [];

% Pre-allocate the slices array (it is too large
% but we will trim at the end)
slices(total_slices).time = [];
slices(total_slices).channels = [];
slices(total_slices).pos = {};

% Traverse list of children
fp = Tiff(fname, 'r');
for slice_cur_idx=1:total_slices
    im_data = fp.read();

    slices(slice_cur_idx).time = slice_cur_idx;
    slices(slice_cur_idx).pos{1} = slice_cur_idx;

    % Preallocate channels array (it is too large
    % but we will trim it at the end)
    clear channels;
    channels.ch = []; %#ok<AGROW>
    channels.image = []; %#ok<AGROW>
    channel_cur_idx = 1;
    
    %TODO: implement multiple channels
    channels(channel_cur_idx).ch = 1; %#ok<AGROW>
    channels(channel_cur_idx).image = single(im_data); %#ok<AGROW>
    channel_cur_idx = channel_cur_idx + 1;
    
    % Trim the channels array
    if channel_cur_idx <= 1
        channels = [];
    else
        clear tmp;
        tmp(channel_cur_idx-1).ch = []; %#ok<AGROW>
        tmp(channel_cur_idx-1).image = []; %#ok<AGROW>
        tmp(1:channel_cur_idx-1) = channels(1:channel_cur_idx-1);
        channels = tmp;
    end
    
    % Check that channels list is unique
    if ~isempty(channels)
        frame_ch_list = [channels(:).ch];
        if length(frame_ch_list) < length(unique(frame_ch_list))
            error 'Frame element contains redundant channel entries';
        end
        
        ch_list = unique([ch_list, frame_ch_list]);
    end
    
    slices(slice_cur_idx).channels = channels;
    
    if fp.lastDirectory()
        if slice_cur_idx ~= total_slices
            error 'Input/output error while reading TIFF file';
        end
    else   
        fp.nextDirectory();
    end
end

close(fp);

obj.slices = slices;
obj.ch_list = ch_list;
obj.pos_lens = 1;

end
