function boxcarFilter(obj, filter_size, scale_factor)
%BOXCARFILTER Apply a boxcar filter to the image data
%   This function applies a boxcar filter to each slice in the
%   dataset. The boxcar filter has a size of 'filter_size'.

for slice_idx=1:length(obj.slices)

    % Traverse each channel in slice
    for ch_idx=1:length(obj.slices(slice_idx).channels)
        im = obj.slices(slice_idx).channels(ch_idx).image;
        im = imboxfilt(im, filter_size);
        im = imresize(im, scale_factor, 'nearest');
        obj.slices(slice_idx).channels(ch_idx).image = im;
    end
    
end

end