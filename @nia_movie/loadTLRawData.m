function loadTLRawData(obj, data_dir)
%LOADTLRAWDATA Read movie from ThorLabs Raw Data
%TODO

% Check input arguments
if ~isrow(data_dir) || ~ischar(data_dir) || isempty(data_dir)
    error 'The argument ''fname'' has an invalid type';
end

xml_fname = [data_dir, filesep, 'Experiment.xml'];

% Destroy the movie
obj.slices = [];
obj.ch_list = [];
obj.pos_lens = [];
obj.pos_lu = {};
obj.pos_ranges = {};
obj.hist_info = [];

% Retrieve image data from XML file
try
    root = xmlread(xml_fname);
catch
    error 'Unable to parse XML file';
end

% Search for the ThorImageExperiment element
if ~root.hasChildNodes
    return;
end

top_children = root.getChildNodes;
for idx=1:top_children.getLength
    if strcmp(top_children.item(idx-1).getNodeName, 'ThorImageExperiment')
        tiexper_root = top_children.item(idx-1);
        break;
    end
end

% Get nodes of the  element
if ~tiexper_root.hasChildNodes
    return;
end

tiexper_children = tiexper_root.getChildNodes;

% Retreive image parameters
im_x_npoints = NaN;
im_y_npoints = NaN;
im_z_npoints = NaN;
pixel_xy_size = NaN;
lsm_framerate = NaN;
pixel_z_size = NaN;
pixel_t_interval = NaN;
chs_enabled = NaN;
num_frames = NaN;
flyback_frames = NaN;

for tiexper_idx=1:tiexper_children.getLength
    
    cur_node = tiexper_children.item(tiexper_idx-1);
    
    if cur_node.getNodeType == cur_node.ELEMENT_NODE
        switch string(cur_node.getNodeName)
            case 'LSM'
                vals = getAttributes(cur_node, {'pixelX', 'pixelY', 'pixelSizeUM'});
                [im_x_npoints, im_y_npoints, pixel_xy_size] = deal(vals{:});
                
                lsm_framerate = getAttribute(cur_node, 'frameRate');
                lsm_framerate = char(lsm_framerate);
                if strcmp(lsm_framerate(end-3:end), ' fps')
                    lsm_framerate = lsm_framerate(1:end-4);
                end
                    
                lsm_framerate = str2double(lsm_framerate);
            case 'ZStage'
                vals = getAttributes(cur_node, {'steps', 'stepSizeUM', 'enable'});
                [im_z_npoints, pixel_z_size, z_enable] = deal(vals{:});
            case 'Timelapse'
                vals = getAttributes(cur_node, {'timepoints', 'intervalSec'});
                [im_t_npoints, pixel_t_interval] = deal(vals{:});
            case 'PMT'
                vals = getAttributes(cur_node, {'enableA', 'enableB', ...
                    'enableC', 'enableD'});
                chs_enabled = cell2mat(vals) > 0;
            case 'Streaming'
                vals = getAttributes(cur_node, {'frames', 'flybackFrames', 'enable', 'zFastEnable'});
                [num_frames, flyback_frames, stream_enable, stream_zfast_enable] = deal(vals{:});
            otherwise
                continue;
        end
    end
end

if isnan(im_x_npoints) || isnan(im_y_npoints) || isnan(pixel_xy_size) || ...
   isnan(lsm_framerate) || isnan(im_z_npoints) || isnan(pixel_z_size) || ...
   isnan(z_enable) || isnan(im_t_npoints) || isnan(pixel_t_interval) || ...
   nnz(isnan(chs_enabled)) > 0 || isnan(num_frames) || isnan(flyback_frames) || ...
   isnan(stream_enable) || isnan(stream_zfast_enable)
    error 'Missing attributes in XML file';
end

% if streaming is enabled, then setting here are dominant
if stream_enable
    % if zfast is disabled, then force one z-plane
    if stream_zfast_enable == 0
        im_z_npoints = 1;
    end
    
    % if there is only one z-plane, thorlabs reports flyback frames, but
    % does not actually perform them, so override that setting here.
    if im_z_npoints == 1
        flyback_frames = 0;
    end
else
    % if zstage is disabled, then force one z-plane
    if z_enable == 0
        im_z_npoints = 1;
    end
    
    % if we are not doing streaming, then disable flyback frames
    flyback_frames = 0;
end

if num_frames ~= (im_z_npoints + flyback_frames) * im_t_npoints
    error 'Attribute Streaming>frames is inconsistent';
end

clear root top_children tiexper_root tiexper_children cur_node;

% Pre-allocate the slices data
slices(num_frames).time = [];
slices(num_frames).channels = [];
slices(num_frames).pos = {};

% Read in the actual data values
if exist([data_dir, filesep, 'Image_0001_0001.raw'], 'file')
    raw_fname = [data_dir, filesep, 'Image_0001_0001.raw'];
elseif exist([data_dir, filesep, 'Image_001_001.raw'], 'file')
    raw_fname = [data_dir, filesep, 'Image_001_001.raw'];
else
    error 'Unable to locate raw data file';
end

raw_fid = fopen(raw_fname, 'rb');
if raw_fid == -1
    error 'Unable to open raw data file';
end

num_channels = nnz(chs_enabled);
ch_idents = 1:length(chs_enabled);
ch_idents = ch_idents(chs_enabled);

slice_idx = 1;
for t_idx=1:im_t_npoints
    for z_idx=1:im_z_npoints
        % Allocate channel data array
        ch_data = [];
        ch_data(num_channels).ch = []; %#ok<AGROW>
        ch_data(num_channels).image = []; %#ok<AGROW>

        for ch_idx=1:num_channels 
            [raw_vals, nelem] = fread(raw_fid, im_x_npoints * im_y_npoints, 'uint16');
            if nelem < im_x_npoints * im_y_npoints
                break;
            end
            
            im = reshape(raw_vals, im_x_npoints, im_y_npoints)';

            ch_data(ch_idx).ch = ch_idents(ch_idx);
            ch_data(ch_idx).image = single(im);
        end

        slices(slice_idx).time = (t_idx-1)*im_z_npoints/lsm_framerate;
        slices(slice_idx).channels = ch_data;
        slices(slice_idx).pos{1} = slice_idx;
        slices(slice_idx).pos{2} = [z_idx, t_idx];
        slice_idx = slice_idx + 1;
    end
    
    % skip over flyback frames
    for fly_idx=1:flyback_frames
        [~, nelem] = fread(raw_fid, im_x_npoints * im_y_npoints, 'uint16');
        if nelem < im_x_npoints * im_y_npoints
            break;
        end
    end    
end

fclose(raw_fid);

% Trim slices if above code ended early
if slice_idx <= length(slices)
    slices(slice_idx:end) = [];
end


% Create position lengths entry
pos_lens_entry = [1, 2];

% Push results into movie object
obj.slices = slices;
obj.ch_list = ch_idents;
obj.pos_lens = pos_lens_entry;

end

function y = getAttributes(node, names)
attrib_list = node.getAttributes;
    
y = cell(1, length(names));
[y{:}] = deal(NaN);

for idx=1:attrib_list.getLength
    cur_attrib = attrib_list.item(idx-1);
    
    cur_name = string(cur_attrib.getName);
    matches = arrayfun(@(x) strcmp(cur_name, x), names);
    match_idx = find(matches, 1, 'first');
    
    if isempty(match_idx)
        continue;
    end
    
    a_str = cur_attrib.getValue;
    ch_val = str2double(a_str);
    
    if isempty(ch_val)
        error 'Invalid attribute encountered';
    end
    
    y{match_idx} = ch_val;
end

end

