function nia_playUFMFTrajMovie(filename, trajectory, ok_snr, acceptROIs)
%NIA_PLAYUFMFTRAJMOVIE Play the movie with the passed filename.
%   nia_playUFMFTrajMovie(filename, trajectory) displays the passed movie.
%   The argument filename must be a path to a UFMF movie. The argument
%   trajectory must be a 2xNxM array, where N is the number of trajectories
%   and M is the number of frames. The argument ok_snr must be a 2xNxM
%   boolean array that is true if the snr is okay, and false otherwise.
%   The format of ok_snr should match trajectory. The argument acceptROIs
%   may be omitted, or it may contain a function handle that should be
%   invoked prior to closing the window. This function returns the figure
%   handle for the main window.
%

% TODO: The histogram function is not currently implemented

% Check arguments
if nargin < 4
    acceptROIs = [];
end

if ~ischar(filename) || ~ismatrix(filename) || ...
        size(filename,1) ~= 1 || isempty(filename)
    error 'The argument ''filename'' has an invalid type';
end

if ~(isempty(acceptROIs) || isa(acceptROIs, 'function_handle'))
    error 'The argument ''acceptROIs'' has an invalid value';
end

callback_list.getImage = @(mov, pos, cmap) getImage(trajectory, ok_snr, mov, pos, cmap);
callback_list.getChannels = @getChannels;
callback_list.getPosRanges = @getPosRanges;
callback_list.getHistInfo = @getHistInfo;
callback_list.processROIs = @processROIs;
callback_list.acceptROIs = acceptROIs;

mov = nia_UFMFMovieHandle;
mov.filename = filename;
mov.header = ufmf_read_header(filename);

nia_playGenericMovie(mov, callback_list);
end

function [im, time] = getImage(trajectory, ok_snr, mov, pos, colormap_spec)
% This function retrieves a single frame from the movie.

disc_radius = 5;
disc_color_good = [0.85, 0, 1];
disc_color_bad = [1, 0.85, 0];

[im, mov.header, timestamp] = ufmf_read_frame(mov.header, pos, 0);

spec = colormap_spec(1);

accum = nia_applyColormapUint8(im, ...
     [spec.min, spec.max], ...
     spec.colormap, spec.nan_color);

accum(accum < 0) = 0;
accum(accum > 1) = 1;

im = accum;
time = timestamp;

% Plot disc on trajectory point
[im_y, im_x] = ndgrid(1:size(im,1), 1:size(im,2));
for area_idx=1:size(trajectory,2)
    mask = (im_x - trajectory(1,area_idx,pos)).^2 + ...
        (im_y - trajectory(2,area_idx,pos)).^2 < disc_radius^2;
    
    if ok_snr(area_idx, pos)
        this_color = disc_color_good;
    else
        this_color = disc_color_bad;
    end
    
    for ch_idx=1:3
        ch = im(:,:,ch_idx);
        ch(mask) = this_color(ch_idx);
        im(:,:,ch_idx) = ch;
    end
end


end

function out = getChannels(~)
% This function retrieves the list of channel identifiers

    out = 1;
end

function out = getPosRanges(mov)
% This function retieves the valid position ranges

    out = [1; mov.header.nframes];
end

function out = getHistInfo(~)
% This function retrieves information on the pixel
% intensity histgrams for the movie.

% TODO: For the moment, this returns a dummy value

hist_info(1).min = 0;
hist_info(1).max = 255;

hist_bins = 64;
hist_edges = linspace(0, 255, hist_bins+1);
dist(1,:) = 0.5*(hist_edges(1:end-1) + hist_edges(2:end));
dist(2,:) = ones(1, hist_bins);
hist_info(1).dist = dist;

out = hist_info;

end

function dset = processROIs(mov, roi_list)
% This function processes the passed ROIs against the
% passed movie in order to produce a cell array of time
% series

dset = cell(1, length(roi_list));

for idx=1:length(roi_list)
    mask = createMask(roi_list(idx).handle);
    
    nframes = mov.header.nframes;
    vec = zeros(2, nframes);
    
    vec(1,:) = mov.header.timestamps;
    
    for frame=1:nframes
        im = ufmf_read_frame(mov.header, frame, 0);
        vec(2,frame) = mean(im(mask));
    end
    
    dset{idx} = vec;
end

end
